import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_animated_button/flutter_animated_button.dart';

void main() => runApp(const GetMaterialApp(home: Home()));

class Home extends StatelessWidget{
  const Home({super.key});

  @override 
  Widget build(context){
    return Scaffold(
      appBar: AppBar(
        title: const Text('Change Color Button'),
        centerTitle: true,
      ),
      body: Center(child: Container( padding: const EdgeInsets.all(18.0),color:  Colors.blueAccent,height: 200,width: 200,alignment: Alignment.center,
        child: AnimatedButton(text: 'SUBMIT',height: 70,width: 150,transitionType: TransitionType.CENTER_TB_OUT,isReverse: true,
        selectedTextColor: Colors.black,backgroundColor: Colors.amber,borderColor: const Color.fromARGB(255, 172, 5, 5),borderRadius: 10,borderWidth: 5,
        textStyle: const TextStyle(
          fontSize: 30,letterSpacing: 5,color: Color.fromARGB(255, 6, 15, 119),fontWeight: FontWeight.w300,
        ),
        onPress: () {
          
        },

        ),
      ),
    )
    );
  }
}


